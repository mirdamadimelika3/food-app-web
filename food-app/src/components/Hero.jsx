import React from "react";
import medical from "../../src/assets/medical.png"

const Hero = () => {
  return (
    <div className="max-w-[1640px] mx-auto p-4">
      <div className="max-h-[800px] relative">
        {/* Overlay */}
        <div className="absolute h-full w-full text-gray-200 max-h-[500px] bg-black/30 flex flex-col justify-center items-center">
          <h1 className="px-4 text-2xl sm:text-3xl md:text-4xl lg:text-5xl ">
            <span className="text-[#a3e635] font-bold">Atlas</span> is your companion 
          </h1>
          <h1 className="px-4 text-2xl sm:text-3xl md:text-4xl lg:text-5xl ">
          in your <span className="text-[#a3e635] font-bold">Health</span> journey
          </h1>
        </div>
        <img className="w-full max-h-[500px] object-cover " src={medical} alt="medical" />
      </div>
    </div>
  );
};

export default Hero;
