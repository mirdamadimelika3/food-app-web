import React, { useState } from "react";
import { data } from "../data/data.js";
const Products = () => {
  // console.log(data)
  const [product, setproduct] = useState(data);

  // Filter Type
  const FilterType = (category) => {
    setproduct(
      data.filter((item) => {
        return item.category === category;
      })
    );
  };

  return (
    <div className="max-w-[1640px] m-auto px-4 py-12">
      <h1 className="text-[#4d7c0f] font-bold text-4xl text-center">
        Atlas Medical Products
      </h1>

      {/* Filter Row */}

      {/* Filter Type */}
      <p className="font-bold text-gray-700">Filter Type</p>
      <div className="flex justify-start  gap-6">
        <div>
          <button
            onClick={() => setproduct(data)}
            className=" m-4 border-[#84cc16] text-[#4d7c0f] font-bold hover:bg-[#84cc16] hover:text-white">
            All
          </button>
          <button
            onClick={() => FilterType("special products")}
            className=" m-4 border-[#84cc16] text-[#4d7c0f] font-bold hover:bg-[#84cc16] hover:text-white">
            Special equipment
          </button>
          <button
            onClick={() => FilterType("rehabilitationl")}
            className=" m-4 border-[#84cc16] text-[#4d7c0f] font-bold hover:bg-[#84cc16] hover:text-white">
            Rehabilitationl
          </button>
          <button
            onClick={() => FilterType("Sports equipment")}
            className=" m-4 border-[#84cc16] text-[#4d7c0f] font-bold hover:bg-[#84cc16] hover:text-white">
            Sports equipment
          </button>
        </div>
      </div>

      {/* Display Images */}
      <div className="grid grid-cols-1 md:grid-cols-2  lg:grid-cols-4  gap-5 pt-4">
        {product.map((item, index) => (
          <div
            key={index}
            className="border shadow-2xl rounded-lg hover:scale-105 duration-300">
            <img
              src={item.image}
              alt={item.name}
              className="w-[100%] h-[350px] object-cover rounded-t-lg"
            />
            <div className="flex justify-between px-3 py-3">
              <p className="font-bold text-base">{item.name}</p>
              <span
                className=" font-black
                 text-[#4d7c0f] p-1  text-sm">
                {item.ptice}
              </span>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Products;
