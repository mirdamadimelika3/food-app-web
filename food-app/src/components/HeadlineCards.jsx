import React from "react";
import Adv1 from "../../src/assets/1.png";
import Adv2 from "../../src/assets/4.png";
import Adv3 from "../../src/assets/2.png";
import Adv4 from "../../src/assets/3.png";


const HeadlineCards = () => {
  return (
    <div className="max-w-[1460px] mx-auto p-4 py-24 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
        
      {/* Card */}
      <div className="border-2 border-[#65a30d] bg-[#ecfccb]  cursor-pointer rounded-2xl  flex flex-col justify-center items-center">
        <div className="flex flex-col justify-center items-center ">
          <p className="text-xs md:text-base lg:text-xl">Advantage 1</p>
          <img className="" src={Adv1} alt="" />
          <span className="text-xs md:text-base lg:text-lg">
            Customized OEM/ODM Manufacturing
          </span>
        </div>
      </div>
      {/* Card */}
      <div className="border-2 border-[#65a30d]  cursor-pointer rounded-2xl  flex flex-col justify-center items-center">
        <div className="flex flex-col justify-center items-center ">
          <p className="text-xs md:text-base lg:text-xl">Advantage 2</p>
          <img className="" src={Adv2} alt="" />
          <span className="text-xs md:text-base lg:text-lg">
            One-stop Diversified Services
          </span>
        </div>
      </div>
      {/* Card */}
      <div className="border-2 border-[#65a30d] bg-[#ecfccb] cursor-pointer rounded-2xl  flex flex-col justify-center items-center">
        <div className="flex flex-col justify-center items-center ">
          <p className="text-xs md:text-base lg:text-xl">Advantage 3</p>
          <img className="" src={Adv3} alt="" />
          <span className="text-xs md:text-base lg:text-lg">
          Excellent Communication

          </span>
        </div>
      </div>
       {/* Card */}
       <div className="border-2 border-[#65a30d]  cursor-pointer rounded-2xl  flex flex-col justify-center items-center">
        <div className="flex flex-col justify-center items-center ">
          <p className="text-xs md:text-base lg:text-xl">Advantage 4</p>
          <img className="" src={Adv4} alt="" />
          <span className="text-xs md:text-base lg:text-lg">
          Made in Taiwan, Globally Trusted

          </span>
        </div>
      </div>
    </div>
  );
};

export default HeadlineCards;
