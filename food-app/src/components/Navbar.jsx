import React, { useState } from "react";
import { AiOutlineMenu, AiOutlineSearch, AiOutlineClose } from "react-icons/ai";
import { BsFillCartFill } from "react-icons/bs";
import { AiOutlineHome } from "react-icons/ai";
import { MdOutlinePersonSearch } from "react-icons/md";
import { BiPlusMedical } from "react-icons/bi";
import { GrContactInfo } from "react-icons/gr";

const Navbar = () => {
  const [nav, setNav] = useState(false);
  return (
    <div className="max-w-[1640px] mx-auto flex justify-between items-center p-4">
      {/* left Side */}
      <div className="flex items-center">
        <div onClick={() => setNav(!nav)} className="cursor-pointer">
          <AiOutlineMenu size={30} />
        </div>
        <h1 className="text-2xl sm:text-3xl lg:text-4xl px-2">
          <span className="font-bold  text-[#65a30d]">َAtlas</span> Medical
        </h1>
        
      </div>
      {/*  Search Input  */}
      <div className="bg-gray-300 rounded-full flex items-center px-2 w-[200px] sm:w-[400px] lg:w-[500px]   ">
        <AiOutlineSearch size={25} />
        <input
          className="bg-transparent p-2 focus:outline-none w-full"
          type="text"
          placeholder="Search Medical Equipment"
        />
      </div>
      {/*  Cart Button  */}
      <button className="bg-[#a3e635] text-white hidden md:flex items-center py-2 rounded-full border-none">
        <BsFillCartFill size={20} className="mr-2" /> cart
      </button>
      {/*  Mobile Menu  */}
      {/* Overlay */}
      {nav ? (
        <div className="bg-black/80 fixed w-full h-screen z-10 top-0 left-0"></div>
      ) : (
        ""
      )}

      {/* side drawer menu */}

      <div
        className={
          nav
            ? "fixed top-0 left-0 w-[300px] h-screen bg-[#f3f4f6] z-10 duration-300"
            : "fixed top-0 left-[-100%] w-[300px] h-screen bg-[#f3f4f6] z-10 duration-300"
        }>
        <AiOutlineClose
          onClick={() => setNav(!nav)}
          size={25}
          className="absolute right-4 top-4 cursor-pointer duration-700"
        />
        <h2 className="text-3xl p-4">
          {" "}
          <span className="text-3xl font-bold text-[#65a30d]">Atlas</span> Medical
        </h2>
        <nav>
          <ul className="flex flex-col p-4 text-gray-800">
            <li className="text-2xl p-4 flex items-center cursor-pointer ">
              <AiOutlineHome className="mr-4" size={20} /> Home
            </li>
            <li className="text-2xl p-4 flex items-center cursor-pointer">
              <MdOutlinePersonSearch className="mr-4" size={20} /> About US
            </li>
            <li className="text-2xl p-4 flex items-center cursor-pointer ">
              <BiPlusMedical className="mr-4" size={20} /> Prudoct
            </li>
            <li className="text-2xl p-4 flex items-center cursor-pointer ">
              <GrContactInfo className="mr-4" size={20} /> Contact US
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
